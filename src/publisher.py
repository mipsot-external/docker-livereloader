import asyncio
from concurrent.futures import Future
from functools import partial
import logging
import os
import time

from cachetools import TTLCache
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer

from .config import LOOP, WATCHDOG_TOLERANCE_WINDOW

logger = logging.getLogger('publisher')
tolerance_window = TTLCache(maxsize=2**15, ttl=WATCHDOG_TOLERANCE_WINDOW)


async def call_at(when, callback, *args, **kwargs):
    if kwargs:
        callback = partial(callback, **kwargs)
    async def coro():
        return await LOOP.run_in_executor(None, callback, *args)
    LOOP.call_at(when, lambda: asyncio.create_task(coro()))


class RestartPublisher(set):
    def dispatch(self, action, *args, **kwargs):
        logger.info(
            f'{self!r} dispatched {action} for {len(self)} subscribers'
        )
        # TODO: depends_on resolver
        for subscriber in self:
            now = LOOP.time()
            at, tw_counter = now, 0
            tw_delay_started, tw_counter = tolerance_window.get(
                (subscriber, action),
                (None, 0)
            )
            if tw_delay_started is not None:
                at = now + WATCHDOG_TOLERANCE_WINDOW
                now = tw_delay_started
            tolerance_window[(subscriber, action)] = now, tw_counter + 1
            if tw_counter < 2:
                if at > LOOP.time():
                    logger.debug(
                        f'Sheduling dispatch {action} for {subscriber} '
                        f'after {at - LOOP.time()} sec'
                    )
                asyncio.run_coroutine_threadsafe(
                    call_at(at, subscriber.dispatch, action, *args, **kwargs),
                    LOOP
                )


class _FileEventHandler(PatternMatchingEventHandler):
    def __init__(self, event_handler, filepath):
        super().__init__(patterns=[filepath])
        self.nested_event_handler = event_handler

    def on_any_event(self, event):
        self.nested_event_handler.dispatch(event)

    def on_moved(self, event):
        self.nested_event_handler.dispatch(event)

    def on_created(self, event):
        self.nested_event_handler.dispatch(event)

    def on_deleted(self, event):
        self.nested_event_handler.dispatch(event)

    def on_modified(self, event):
        self.nested_event_handler.dispatch(event)


class WatchdogPublisher():
    def __init__(self, subscribers):
        super().__init__()
        logger.debug(f'{self!r} initializing')
        self.subscribers = subscribers
        self.observer = None

    def open(self):
        logger.debug(
            f'{self!r} opening with {len(self.subscribers)} subscribers'
        )
        self.observer = Observer()
        scheduled_paths = set()
        for subscriber in self.subscribers:
            for path in subscriber.paths:
                scheduled_paths.add(path)
                kwargs = {
                    True: dict(
                        event_handler=subscriber.event_handler,
                        path=str(path),
                        recursive=True
                    ),
                    False: dict(
                        event_handler=_FileEventHandler(
                            subscriber.event_handler,
                            str(path)
                        ),
                        path=os.path.dirname(path)
                    )
                }
                self.observer.schedule(**kwargs[os.path.isdir(path)])
        logger.info(
            f'{self!r} sheduled {len(scheduled_paths)} paths '
            f'for {len(self.subscribers)} subscribers'
        )
        self.observer.start()

    def close(self):
        logger.info(f'{self!r} closing')
        self.observer.stop()
        self.observer.join()
        self.observer = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
