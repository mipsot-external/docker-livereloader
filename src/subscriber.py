import logging
import time

import docker
from timeout_decorator import timeout as timeout_decorator
from watchdog.events import PatternMatchingEventHandler

from .config import RESTART_TIMEOUT, WATCHDOG_SLEEP
from .helpers import docker_inspector
from .publisher import RestartPublisher

logger = logging.getLogger('subscriber')


class ContainerSubscriber(str):
    @timeout_decorator(RESTART_TIMEOUT, use_signals=False)
    def dispatch(self, action):
        logger.info(f'{self!r} dispatched {action}')
        if action == 'restart':
            container = docker.from_env().containers.get(self)
            if container.status == 'running':
                container.restart()
        else:
            raise ValueError(
                f'Unknown dispatched action {action} for {self[:12]} container'
            )

    def __repr__(self):
        cls_name = self.__class__.__name__
        return f'<{cls_name} at {id(self)} for {self[:12]} container>'


class AnyFileEventHandler(PatternMatchingEventHandler):
    def __init__(self, targets, ignore_directories=True, **kwargs):
        super().__init__(
            ignore_directories=ignore_directories,
            case_sensitive=True,
            **kwargs
        )
        self.targets = targets
        self.restart_publisher = RestartPublisher()

    @property
    def target_containers(self):
        containers = set()
        for target in self.targets:
            target_type = target['target_type']
            if target_type != 'container':
                raise ValueError(f'Unknown target type {target_type}')
            name_scope = target.get('name_scope', 'auto')
            matched_containers, docker_client = set(), docker.from_env()
            for name_regex in (
                target['name_regex'],
                docker_inspector.wrap_container_name(
                    name_scope,
                    target['name_regex']
                )
            ):
                matched_containers |= set(
                    docker_client.containers.list(filters={
                        'name': f'^{name_regex}$',
                        'status': ['restarting', 'running']
                    })
                )
                logger.debug(
                    f'{self!r} got {len(matched_containers)} containers '
                    f"using '{name_regex}' in {name_scope} scope"
                )
            matched_containers &= docker_inspector.container_scope(name_scope)
            containers |= matched_containers
        return containers

    def restart(self):
        logger.debug(f'{self!r} will restart, getting target containers')
        subscribers = {
            ContainerSubscriber(c.id)
            for c in self.target_containers
        }
        if len(subscribers) > 1:  # NOTE: self restarting should be in separate rule
            subscribers -= {docker_inspector.self_container_id}
        logger.debug(
            f'{self!r} found {len(subscribers)} target containers: '
            f'{subscribers}'
        )
        logger.debug(
            f'{self!r} purge '
            f'{len(self.restart_publisher - subscribers)} containers'
        )
        # NOTE: &= will break SubscriberProxy
        self.restart_publisher -= \
            self.restart_publisher - subscribers
        logger.debug(
            f'{self!r} add '
            f'{len(subscribers - self.restart_publisher)} containers'
        )
        self.restart_publisher |= subscribers
        self.restart_publisher.dispatch('restart')

    def on_any_event(self, event):
        logger.debug(f'{self!r} got {event}. Sleep {WATCHDOG_SLEEP} sec')
        time.sleep(WATCHDOG_SLEEP)
        logger.debug(
            f'{self!r} restarting after {WATCHDOG_SLEEP} sec sleeping'
        )
        self.restart()


class WatchdogSubscriber():
    def __init__(self, targets, observables):
        super().__init__()
        self.event_handler = AnyFileEventHandler(targets)
        self.paths = [observable['path'] for observable in observables]
