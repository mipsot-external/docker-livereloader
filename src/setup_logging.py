import logging
import os
import sys
import time

CUTOFF_LEVEL = os.environ.get('LOGGING_LEVEL', logging.INFO)


class InfoFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno < logging.WARNING


if not logging.root.hasHandlers():
    logging.root.setLevel(CUTOFF_LEVEL)
    logging.Formatter.converter = time.gmtime  # asctime in UTC
    formatter = logging.Formatter(
        '%(asctime)sZ [%(levelname)s %(name)s]: %(message)s'
    )
    out_handler = logging.StreamHandler(sys.stdout)
    out_handler.setLevel(CUTOFF_LEVEL)
    out_handler.addFilter(InfoFilter())
    out_handler.setFormatter(formatter)
    logging.root.addHandler(out_handler)
    err_handler = logging.StreamHandler()
    err_handler.setLevel(logging.WARNING)
    err_handler.setFormatter(formatter)
    logging.root.addHandler(err_handler)
