import asyncio
import logging
import os
from contextlib import suppress

import yaml

with suppress(ModuleNotFoundError):
    import dotenv
    dotenv.load_dotenv()

logger = logging.getLogger('config')

logger.debug('Setup mount paths')
AUTO_OBSERVABLES_PATH = os.environ.get(
    'DLR_AUTO_OBSERVABLES_PATH',
    '/run/dlr/observables/auto'
)
DC_PROJECT_PATH = os.environ.get('DLR_DC_PROJECT_PATH', '/run/dlr/project')

# pylint: disable=wrong-import-position
from .helpers import docker_inspector

# TODO: add rebuilding of images


logger.debug('Setup timeouts')
RESTART_TIMEOUT = \
    None \
    if os.environ.get('DLR_RESTART_TIMEOUT') is None \
    else int(os.environ.get('DLR_RESTART_TIMEOUT'))
WATCHDOG_SLEEP = float(os.environ.get('DLR_WATCHDOG_SLEEP', '1.5'))
WATCHDOG_TOLERANCE_WINDOW = int(
    os.environ.get('DLR_WATCHDOGTOLERANCE_WINDOW', '60')
)

logger.debug('Setup DLR_CONFIG')
_config = yaml.safe_load(os.environ.get('DLR_CONFIG', '{}'))
logger.debug('Setup trigger rules')
_trigger_rules = [
    *_config.get('trigger_rules', []),
    *([{
        'targets': [{
            'target_type': 'container',
            'name_scope': 'auto',
            'name_regex': '.*'
        }],
        'observables': docker_inspector.auto_observables
    }] if docker_inspector.auto_observables else [])
]
TRIGGER_RULES = [
    {
        **rule,
        'targets': [
            {
                **target,
                'name_scope':
                    (
                        'docker-compose'
                        if docker_inspector.dc_mounted
                        else 'docker'
                    )
                    if target.get('name_scope', 'auto') == 'auto'
                    else target['name_scope']
            }
            for target in rule['targets']
        ]
    }
    for rule in _trigger_rules
]
logger.info(f'Found {len(TRIGGER_RULES)} rules')

logger.debug('Setup master loop')
LOOP = asyncio.get_event_loop()

logger.debug('Setup process finished')
