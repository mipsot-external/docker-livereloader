import time

from . import setup_logging
from .config import LOOP, TRIGGER_RULES
from .publisher import WatchdogPublisher
from .subscriber import WatchdogSubscriber


def main():
    subscribers = [
        WatchdogSubscriber(**rule)
        for rule in TRIGGER_RULES
    ]
    with WatchdogPublisher(subscribers):
        LOOP.run_forever()