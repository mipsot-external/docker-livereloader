import logging
import os
from pathlib import Path

import docker
import yaml

try:
    from functools import cached_property
except ModuleNotFoundError:
    from cached_property import cached_property

from .config import AUTO_OBSERVABLES_PATH, DC_PROJECT_PATH

logger = logging.getLogger('helpers')


class DockerInspector():
    @cached_property
    def dc_mounted(self):
        for test_dc_yml_path in self.self_mount_paths:
            if test_dc_yml_path.name != 'docker-compose.yml':
                continue
            parents = iter(test_dc_yml_path.parents)
            try:
                test_project_path = next(parents)
            except StopIteration:
                continue
            try:
                test_dc_project_path = next(parents)
            except StopIteration:
                continue
            if test_dc_project_path != Path(DC_PROJECT_PATH):
                continue
            break
        else:
            return False
        return True

    @cached_property
    def dc_yml_path(self):
        [dc_project_name] = next(os.walk(DC_PROJECT_PATH))[1]
        return Path(DC_PROJECT_PATH) / dc_project_name / 'docker-compose.yml'

    @cached_property
    def auto_observables(self):
        return [
            {'path': mount_path}
            for mount_path in self.self_mount_paths
            if Path(AUTO_OBSERVABLES_PATH) in (mount_path, *mount_path.parents)
        ]

    @cached_property
    def self_container_id(self):
        logger.debug('Getting container id')
        with open('/proc/self/cgroup', 'rt') as f:
            for line in f:
                num_str, spec, path = line.strip('\n').split(':')
                if num_str != '1':
                    continue
                return Path(path).name

    @cached_property
    def self_mount_paths(self):
        logger.debug('Getting container mounts')
        self_container = docker \
            .from_env() \
            .containers \
            .get(self.self_container_id)
        return [
            Path(mount['Destination']).resolve()
            for mount in self_container.attrs['Mounts']
        ]

    @property
    def docker_compose_yaml(self):
        logger.debug('Load docker-compose file')
        with open(self.dc_yml_path, 'rt') as f:
            return yaml.safe_load(f.read())

    def container_scope(self, scope):
        filters = {'status': ['restarting', 'running']}
        cn_filters = self.container_name_filters(scope)
        if cn_filters is not None:
            filters['name'] = list(cn_filters)
        return set(docker.from_env().containers.list(filters=filters))

    def container_name_filters(self, scope):
        cn_filters = set()
        if scope == 'docker-compose':
            for service in self.docker_compose_yaml['services']:
                _cn_filter = self.wrap_container_name(scope, service)
                if 'container_name' in service:
                    _cn_filter = service['container_name']
                cn_filters.add(f'^{_cn_filter}$')
            return cn_filters
        elif scope == 'docker':
            return None
        raise ValueError(f'Unknown container scope {scope}')

    def wrap_container_name(self, scope, name):
        if scope == 'docker-compose':
            dc_project_name = next(iter(self.dc_yml_path.parents)).name
            # TODO: escape special regex symbols
            return f'{dc_project_name}_{name}_.*'
        return name


docker_inspector = DockerInspector()
