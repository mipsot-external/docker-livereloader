FROM python:3-alpine

COPY requirements.txt /tmp/
RUN pip install --no-cache -r /tmp/requirements.txt && rm -v /tmp/requirements.txt

COPY src /opt/src
WORKDIR /opt
CMD ["python", "-m", "src"]
